// Методи це фунцкції, які зберігаються в властивостях об'єкта
// В значення властивості можна помістити будь-який тип даних
// Якщо скопіювати змінну об'єкта, то скопіюється лише посилання на цей об'єкт а сам він не продублюється.
function createUser(){
    let firstName = prompt("Введіть ваше ім'я:");
    let lastName = prompt("Введіть ваше прізвище:");
    return {
        name: firstName,
        lastname: lastName,
        getLogin: function() {
            return this.name.charAt(0).toLowerCase() + this.lastname.toLowerCase();
        }
    };
}

let newUser = createUser();
console.log(newUser.getLogin());